package by.bsuir.cloudbin.listener;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

import by.bsuir.cloudbin.data.WebData;

/**
 * Initialize web application context path
 * @author Raman_Yelianevich
 */
@WebListener(value = "Initialize application context path")
public final class ContextListener implements ServletContextListener {
	private static final String FILES_FOLDER = "files";
	private static final Logger LOG = Logger.getLogger(ContextListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) { }

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext servletContext = sce.getServletContext();
		String contextPath = servletContext.getRealPath("/");
		String separator = System.getProperty("file.separator");
		String localPath = contextPath + "WEB-INF" + separator + FILES_FOLDER + separator;
		String logsPath = contextPath + "WEB-INF";
		System.setProperty("logPath", logsPath);
		WebData.filesPath = localPath;
		WebData.separator = separator;
		LOG.info("files path: " + localPath);
	}
}
