package by.bsuir.cloudbin.logic;

import java.io.Serializable;

import org.apache.log4j.Logger;

import by.bsuir.cloudbin.model.User;
import by.bsuir.cloudbin.model.Users;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import com.sun.jersey.api.client.filter.HTTPBasicAuthFilter;

/**
 * Service to get information about User 
 * @author Raman Yelianevich
 *
 */
public final class UserService implements Serializable {
	private static final long serialVersionUID = 4140126651509432952L;
	private static final Logger LOG = Logger.getLogger(UserService.class);
	private static final String MEDIA_TYPE = "application/json";
	private static final String HTTP_ERROR_MSG = "Failed : HTTP error code : ";
	private String serviceUsersDetailsUrl;
	private String serviceUserDetailsUrl;
	private String remoteUsername;
	private String remotePassword;
	
	public Users getUsers(User requestUser) {
		Users users = null;
		try {
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter(remoteUsername, remotePassword));
			WebResource webResource = client.resource(serviceUsersDetailsUrl);
			ClientResponse response = webResource.accept(MEDIA_TYPE).get(ClientResponse.class);
			if (response.getStatus() != 200) {
			   throw new RuntimeException(HTTP_ERROR_MSG + response.getStatus());
			}
			String usersJson = response.getEntity(String.class);
			ObjectMapper objectMapper = new ObjectMapper();
			objectMapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
			users = objectMapper.readValue(usersJson, Users.class);
			LOG.info(users);
		  } catch (Exception e) {
			e.printStackTrace();
			users = new Users();
		  }
		return users;
	}
	
	public User getUserData(String username) {
		User userDetails = null;
		try {
			Client client = Client.create();
			client.addFilter(new HTTPBasicAuthFilter(remoteUsername, remotePassword));
			WebResource webResource = client
					.resource(serviceUserDetailsUrl + username);
			
			ClientResponse response = webResource.accept(MEDIA_TYPE)
	                   .get(ClientResponse.class);
	 
			if (response.getStatus() != 200) {
			   throw new RuntimeException(HTTP_ERROR_MSG + response.getStatus());
			}
	 
			String userJson = response.getEntity(String.class);
			ObjectMapper objectMapper = new ObjectMapper();
			userDetails = objectMapper.readValue(userJson, User.class);
			
			LOG.info(userDetails);
	 
		  } catch (Exception e) {
			e.printStackTrace();
			userDetails = new User();
			userDetails.setUsername(username);
		  }
		return userDetails;
	}

	public void setServiceUsersDetailsUrl(String serviceUsersDetailsUrl) {
		this.serviceUsersDetailsUrl = serviceUsersDetailsUrl;
	}

	public void setServiceUserDetailsUrl(String serviceUserDetailsUrl) {
		this.serviceUserDetailsUrl = serviceUserDetailsUrl;
	}
	
	public void setRemoteUsername(String remoteUsername) {
		this.remoteUsername = remoteUsername;
	}

	public void setRemotePassword(String remotePassword) {
		this.remotePassword = remotePassword;
	}
}
