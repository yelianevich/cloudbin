package by.bsuir.cloudbin.logic;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.log4j.Logger;
import org.primefaces.model.UploadedFile;

import by.bsuir.cloudbin.dao.IUserFileDao;
import by.bsuir.cloudbin.data.WebData;
import by.bsuir.cloudbin.model.UserFile;

public final class FileUploader implements Serializable {
	private static final long serialVersionUID = -4951361341861434986L;
	private static final Logger LOG = Logger.getLogger(FileUploader.class);
	private static final int BYTES_PORTION = 1024;
	private IUserFileDao userFileDao;
	
	public void uploadFile(String username, UploadedFile uploadedFile) throws IOException {
		String path = WebData.filesPath + username;
        Path userDir = Paths.get(path);
        if (Files.notExists(userDir, LinkOption.NOFOLLOW_LINKS)) {
        	Files.createDirectory(userDir);
        }
       
        String filename = uploadedFile.getFileName();
        String newsFilePath = userDir + WebData.separator + filename;
        File file = new File(newsFilePath);
        try (InputStream stream = uploadedFile.getInputstream();
	        	BufferedInputStream inputStream = new BufferedInputStream(stream);
        		FileOutputStream fileOutputStream = new FileOutputStream(file);
	        	BufferedOutputStream outputStream = new BufferedOutputStream(fileOutputStream);) {
        	byte[] bytes = new byte[BYTES_PORTION];
        	while (inputStream.read(bytes) != -1) {
        		outputStream.write(bytes);
        	}
        	UserFile userFile = new UserFile(username, filename, uploadedFile.getSize());
        	userFileDao.addUserFile(userFile);
		} catch (IOException e) {
			LOG.error("Error during uploding", e);
			throw new IOException("Error during uploding", e);
		}
	}

	public void setUserFileDao(IUserFileDao userFileDao) {
		this.userFileDao = userFileDao;
	} 
}
