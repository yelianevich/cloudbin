package by.bsuir.cloudbin.security;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import by.bsuir.cloudbin.logic.UserService;

/**
 * Custom implementation of UserDetailsService to get
 * datails about user from REST service
 * @author Raman Yelianevich
 *
 */
public class ServiceUserDetail implements UserDetailsService {
	private UserService userService;
	
	/**
	 * Loads user details from REST user service
	 */
	@Override
	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		UserDetails userDetails = userService.getUserData(username);
		return userDetails;
	}

	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
