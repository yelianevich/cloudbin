package by.bsuir.cloudbin.data;

public final class WebData {
	private WebData() {}
	
	/** Path to folder with users files (Came from context listener) */
	public static String filesPath;
	
	/** System separator (Came from context listener) */
	public static String separator;
}
