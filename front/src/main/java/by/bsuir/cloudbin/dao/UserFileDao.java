package by.bsuir.cloudbin.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import by.bsuir.cloudbin.model.UserFile;

@Repository
public final class UserFileDao implements IUserFileDao {
	private EntityManager entityManager;
	private static final String JPQL_FILENAME_PARAM = "filename";
	private static final String JPQL_SELECT_FILE =
			"SELECT userFile FROM UserFile as userFile WHERE userFile.filename = :" + JPQL_FILENAME_PARAM;
	private static final String JPQL_USERNAME_PARAM = "username";
	private static final String JPQL_SELECT_USERNAME =
			"SELECT userFile FROM UserFile as userFile WHERE userFile.username = :" + JPQL_USERNAME_PARAM;
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	@Override
	@Transactional(readOnly = true)
	public List<UserFile> getUserFiles(String username) {
		TypedQuery<UserFile> query = entityManager.createQuery(JPQL_SELECT_USERNAME, UserFile.class);
		query.setParameter(JPQL_USERNAME_PARAM, username);
		List<UserFile> userFileList = query.getResultList();
		return userFileList;
	}

	@Override
	@Transactional
	public void addUserFile(UserFile userFile) {
		TypedQuery<UserFile> query = entityManager.createQuery(JPQL_SELECT_FILE, UserFile.class);
		String filename = userFile.getFilename();
		query.setParameter(JPQL_FILENAME_PARAM, filename);
		query.setMaxResults(1);
		List<UserFile> userFileList = query.getResultList();
		if(userFileList.size() == 0) {
			entityManager.persist(userFile);
		} else {
			UserFile prevFile = userFileList.get(0);
			userFile.setId(prevFile.getId());
			entityManager.merge(userFile);
		}
		
	}
}
