package by.bsuir.cloudbin.dao;

import java.util.List;

import by.bsuir.cloudbin.model.UserFile;

public interface IUserFileDao {
	public List<UserFile> getUserFiles(String username);
	public void addUserFile(UserFile userFile);
}
