package by.bsuir.cloudbin.beans;

import java.io.IOException;
import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import org.apache.log4j.Logger;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.security.core.context.SecurityContextHolder;

import by.bsuir.cloudbin.logic.FileUploader;
  
@SessionScoped
@ManagedBean(name = "fileUploadController")
public class FileUploadController implements Serializable {  
	private static final long serialVersionUID = -7701827152008208292L;
	private static final Logger LOG = Logger.getLogger(FileUploadController.class);
	
	@ManagedProperty(value="#{fileUploader}")
	private FileUploader fileUploader;
	
    public void handleFileUpload(FileUploadEvent event) {  
    	String username = SecurityContextHolder.getContext().getAuthentication().getName();
    	UploadedFile uploadedFile = event.getFile();
    	FacesMessage msg = null;
    	try {
			fileUploader.uploadFile(username, uploadedFile);
			msg = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
		} catch (IOException e) {
			LOG.error("Was not able to handle file upload", e);
	    	msg = new FacesMessage("Error occured", "Can not upload " + event.getFile().getFileName()); 
		}
    	FacesContext.getCurrentInstance().addMessage(null, msg);
    }

	public void setFileUploader(FileUploader fileUploader) {
		this.fileUploader = fileUploader;
	}
}
