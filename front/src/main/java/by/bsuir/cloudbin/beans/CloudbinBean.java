package by.bsuir.cloudbin.beans;

import java.io.Serializable;
import java.util.List;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import org.springframework.security.core.context.SecurityContextHolder;

import by.bsuir.cloudbin.dao.IUserFileDao;
import by.bsuir.cloudbin.logic.UserService;
import by.bsuir.cloudbin.model.User;
import by.bsuir.cloudbin.model.UserFile;
import by.bsuir.cloudbin.model.UserRole;
import by.bsuir.cloudbin.model.Users;

@SessionScoped
@ManagedBean(name = "cloudbean")
public final class CloudbinBean implements Serializable {
	private static final long serialVersionUID = -7210758811667953564L;
	
	@ManagedProperty(value = "#{userFileDao}")
	private IUserFileDao userFileDao;
	
	@ManagedProperty(value = "#{userService}")
	private UserService userService;
	
	public String getUsername() {
		return SecurityContextHolder.getContext().getAuthentication().getName();
	}
	
	public Users getUsers() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		Users users = userService.getUsers(user);
		return users;
	}
	
	public boolean checkRole(String role) {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		for (UserRole userRole : user.getAuthorities()) {
			if (userRole.getAuthority().equals(role)) {
				return true;
			}
		}
		return false;
	}
	
	public List<UserFile> getUserFiles() {
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		List<UserFile> userFiles = userFileDao.getUserFiles(username);
		return userFiles;
	}
	
	public User getUserData() {
		User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		return user;
	}
	public void setUserFileDao(IUserFileDao userFileDao) {
		this.userFileDao = userFileDao;
	}
	public void setUserService(UserService userService) {
		this.userService = userService;
	}
}
