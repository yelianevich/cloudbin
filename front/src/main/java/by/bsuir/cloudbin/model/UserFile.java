package by.bsuir.cloudbin.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "USER_FILE")
public class UserFile {

	@Id
	@GenericGenerator(name = "fileGen", strategy = "increment")
	@GeneratedValue(generator = "fileGen")
	@Column(name = "ID")
	private Long id;
	
	@Column(name = "FILENAME")
	private String filename;
	
	@Column(name = "USERNAME", nullable = false)
	private String username;
	
	@Column(name = "DATE_MIDIFIED")
	private Date date;

	@Column(name = "FILE_SIZE")
	private long fileSize;
	
	public UserFile() {}
	
	public UserFile(String username, String filename, long size) {
		this.username = username;
		this.filename = filename;
		this.fileSize = size;
		this.date = new Date();
	}
	
	public UserFile(String username, String filename, long size, Date modified) {
		this.username = username;
		this.filename = filename;
		this.fileSize = size;
		this.date = modified;
	}
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getFilename() {
		return filename;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public Date getDate() {
		return date;
	}
	public void setDate(Date date) {
		this.date = date;
	}
	public long getFileSize() {
		return fileSize;
	}
	public void setFileSize(long fileSize) {
		this.fileSize = fileSize;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((date == null) ? 0 : date.hashCode());
		result = prime * result + (int) (fileSize ^ (fileSize >>> 32));
		result = prime * result
				+ ((filename == null) ? 0 : filename.hashCode());
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result
				+ ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserFile other = (UserFile) obj;
		if (date == null) {
			if (other.date != null)
				return false;
		} else if (!date.equals(other.date))
			return false;
		if (fileSize != other.fileSize)
			return false;
		if (filename == null) {
			if (other.filename != null)
				return false;
		} else if (!filename.equals(other.filename))
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "UserFile [id=" + id + ", filename=" + filename + ", username="
				+ username + ", date=" + date + ", fileSize=" + fileSize + "]";
	}
	
	
}
