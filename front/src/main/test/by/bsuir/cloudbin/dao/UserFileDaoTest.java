package by.bsuir.cloudbin.dao;

import java.util.List;

import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import by.bsuir.cloudbin.model.UserFile;

public class UserFileDaoTest {
	private ApplicationContext ctx;
    private IUserFileDao userFileDao;
 
    public UserFileDaoTest() {
        ctx = new ClassPathXmlApplicationContext("CONFIGURATION/SPRING/BEANDEFINITION/db_context.xml");
        userFileDao = (IUserFileDao) ctx.getBean("userFileDao");
        System.out.println("Application Context Loaded");
    }
    
	@Test
	public void test() {
		UserFile userFile = new UserFile("levoner", "951005.txt", 343434);
		userFileDao.addUserFile(userFile);
		List<UserFile> userFiles = userFileDao.getUserFiles("levoner");
		System.out.println(userFiles);
	}
}
