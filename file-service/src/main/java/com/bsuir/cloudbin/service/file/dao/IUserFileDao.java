package com.bsuir.cloudbin.service.file.dao;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import javax.jws.soap.SOAPBinding;
import javax.jws.soap.SOAPBinding.Style;
import javax.jws.soap.SOAPBinding.Use;
import javax.xml.ws.RequestWrapper;

import com.bsuir.cloudbin.service.file.model.UserFile;

@SOAPBinding(style = Style.DOCUMENT, use = Use.LITERAL)
@WebService(name = "filesManager",
		targetNamespace = "http://cloudbin.bsuir.by",
		wsdlLocation = "http://localhost:8087/file-service?wsdl")
public interface IUserFileDao {
	
	@WebMethod(operationName = "getUserFiles", exclude = false)
	public List<UserFile> getUserFiles(@WebParam(name = "username") String username);
	
	@RequestWrapper(className = "com.bsuir.cloudbin.service.file.model.UserFile",
			targetNamespace = "http://cloudbin.bsuir.by/types")
	@WebMethod(operationName = "addUserFile", exclude = false)
	public void addUserFile(@WebParam(name = "userFile") UserFile userFile);
}
