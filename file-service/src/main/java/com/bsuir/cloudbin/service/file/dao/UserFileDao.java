package com.bsuir.cloudbin.service.file.dao;

import java.util.ArrayList;
import java.util.List;

import javax.jws.WebService;

import com.bsuir.cloudbin.service.file.model.UserFile;

@WebService(endpointInterface = "com.bsuir.cloudbin.service.file.dao.IUserFileDao",
		targetNamespace = "http://cloudbin.bsuir.by",
		serviceName = "UserFileService",
		portName = "UserFilePort")
public class UserFileDao implements IUserFileDao {

	@Override
	public List<UserFile> getUserFiles(String username) {
		// TODO Auto-generated method stub
		return new ArrayList<UserFile>();
	}

	@Override
	public void addUserFile(UserFile userFile) {
		// TODO Auto-generated method stub
	}

}
