package by.bsuir.cloudbin.dao;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Test;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;

import by.bsuir.cloudbin.model.User;
import by.bsuir.cloudbin.model.UserRole;

public class UserDaoTest {

	@Test
	public void test() {
		UserDao userDao = new UserDao();
		
		List<User> users = userDao.getAllUsers();
		System.out.println(users);
		try {
			ShaPasswordEncoder encoder = new ShaPasswordEncoder(256);
			
			User user = new User();
			user.setEnabled(true);
			user.setFirstName("r");
			user.setLastName("y");
			String sha256Password = encoder.encodePassword("password", null);
			user.setPassword(sha256Password);
			user.setUsername("levoner");
			Set<UserRole> roles = new HashSet<>();
			UserRole roleUser = new UserRole("ROLE_USER");
			roles.add(roleUser);
			roles.add(new UserRole("ROLE_ADMIN"));
			user.setAuthorities(roles);
			
			userDao.saveOrUpdateUser(user);
			
			User regUser = new User();
			regUser.setEnabled(true);
			regUser.setFirstName("FirstName");
			regUser.setLastName("LastName");
			
			sha256Password = encoder.encodePassword("regpassword", null);
			regUser.setPassword(sha256Password);
			regUser.setUsername("reguser");
			Set<UserRole> rolesRegUser = new HashSet<>();
			rolesRegUser.add(roleUser);
			regUser.setAuthorities(rolesRegUser);

			userDao.saveOrUpdateUser(regUser);
			
			User selectedUser = userDao.selectUserByUsername("levoner");
			System.out.println(selectedUser);
		} catch (DaoException e) {
			e.printStackTrace();
		}
	}

}
