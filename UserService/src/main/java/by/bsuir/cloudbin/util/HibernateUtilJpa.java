package by.bsuir.cloudbin.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.apache.log4j.Logger;

/**
 * Singleton utility class to get JPA EntityManager
 * @author Raman Yelianevich
 *
 */
public enum HibernateUtilJpa {
	INSTANCE;
	
	private static final String CONFIG_PATH = "user-manager"; 
	private static final String CONFIG_ERROR_MSG = "Was not able to initialize SessionFactory configuration";
	private final EntityManagerFactory  entityManagerFactory;
	
	private HibernateUtilJpa() {
		try {
			entityManagerFactory = Persistence.createEntityManagerFactory(CONFIG_PATH);
    	} catch (Throwable e) {
    		Logger log = Logger.getLogger(getClass().getSimpleName());
    		log.error(CONFIG_ERROR_MSG, e);
    		throw new ExceptionInInitializerError(e);
    	}
	}

    public EntityManager getEntityManager() {
    	return entityManagerFactory.createEntityManager();
    }
}
