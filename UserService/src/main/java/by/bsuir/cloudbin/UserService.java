package by.bsuir.cloudbin;

import java.io.IOException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import by.bsuir.cloudbin.dao.DaoException;
import by.bsuir.cloudbin.dao.IUserDao;
import by.bsuir.cloudbin.model.User;
import by.bsuir.cloudbin.model.Users;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * REST user service endpoint
 * @author Raman_Yelianevich
 */
@Component
@Path("/user")
public class UserService {
	private static final Logger LOG = Logger.getLogger(UserService.class);
	private static final String CREATE_ERROR = "User creation failed";
	
	@Autowired
	ObjectMapper objectMapper;
	
	@Autowired
	IUserDao userDao;
	
	@GET
	@Path("/test")
	@Produces(MediaType.TEXT_PLAIN)
	public String test() {
		return "It works";
	}
	
	@GET
	@Path("/details/{username}")
	@Consumes(MediaType.TEXT_PLAIN)
	@Produces(MediaType.APPLICATION_JSON)
	public String auth(@PathParam("username") String username) {
		User user = userDao.selectUserByUsername(username);
		String userJson = null;
		try {
			userJson = objectMapper.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			LOG.error("JSON transformation error", e);
		}
		return userJson;
	}
	
	@GET
	@Path("/{id}")
	@Produces(MediaType.APPLICATION_JSON)
	public String findUserById (@PathParam("id") Long id) {
		User user = userDao.selectUserById(id);
		String userJson = null;
		try {
			userJson = objectMapper.writeValueAsString(user);
		} catch (JsonProcessingException e) {
			LOG.error("JSON transformation error", e);
		}
		return userJson;
	}
	
	@GET
	@Path("/users")
	@Produces(MediaType.APPLICATION_JSON)
	public String getUsers() {
		List<User> usersList = userDao.getAllUsers();
		Users users = new Users(usersList);
		String usersJson = null;
		try {
			usersJson = objectMapper.writeValueAsString(users);
		} catch (JsonProcessingException e) {
			LOG.error("Error during users fetch", e);
		}
		return usersJson;
	}
	
	@POST 
	@Consumes(MediaType.APPLICATION_JSON)
	public String create(User user) {
		String userJson = null;
		try {
			userDao.saveOrUpdateUser(user);
			userJson = objectMapper.writeValueAsString(user);
		} catch (DaoException | JsonProcessingException e) {
			LOG.error(CREATE_ERROR, e);
		}
		return userJson;
	}
	
	@PUT
	@Consumes(MediaType.APPLICATION_JSON)
	public void update(String userJson) {
		try {
			User user = objectMapper.readValue(userJson, User.class);
			userDao.saveOrUpdateUser(user);
			LOG.info("User removed");
		} catch (IOException | DaoException e) {
			LOG.error("Can not read user's JSON");
		}
	}
	
	@DELETE
	@Consumes(MediaType.APPLICATION_JSON)
	public void delete(String usersJson) {
		try {
			Users users = objectMapper.readValue(usersJson, Users.class);
			userDao.deleteUsers(users);
			LOG.info("User removed");
		} catch (IOException | DaoException e) {
			LOG.error("Can not read user's JSON");
		}
	}
}
