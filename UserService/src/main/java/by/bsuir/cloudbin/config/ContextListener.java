package by.bsuir.cloudbin.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;

/**
 * Initialize web application context path
 * @author Raman_Yelianevich
 */
@WebListener(value = "Initialize application context path")
public final class ContextListener implements ServletContextListener {
	private static final Logger LOG = Logger.getLogger(ContextListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent sce) { }

	@Override
	public void contextInitialized(ServletContextEvent sce) {
		ServletContext servletContext = sce.getServletContext();
		String contextPath = servletContext.getRealPath("/");
		String logsPath = contextPath + "WEB-INF";
		System.setProperty("log", logsPath);
		LOG.info("log path: " + logsPath);
	}
}
