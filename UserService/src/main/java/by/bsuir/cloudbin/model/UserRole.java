package by.bsuir.cloudbin.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.springframework.security.core.GrantedAuthority;

/**
 * User's role entity class
 * @author Raman_Yelianevich
 */
@Entity
@Table(name = "user_role")
public class UserRole implements Serializable, GrantedAuthority {
	private static final long serialVersionUID = 1351158457251747049L;
	
	@Id
	@Column(name = "USER_ROLE_ID")
	@GenericGenerator(name = "role_gen", strategy = "increment")  
    @GeneratedValue(generator = "role_gen")  
	private Long userRoleId;
	
	@Column(name = "AUTHORITY", length = 45, nullable = false)
	private String authority;
	
	public UserRole() {}
	
	public UserRole(String authority) {
		this.authority = authority;
	}

	public Long getUserRoleId() {
		return userRoleId;
	}

	public void setUserRoleId(Long userRoleId) {
		this.userRoleId = userRoleId;
	}

	@Override
	public String getAuthority() {
		return authority;
	}

	public void setAuthority(String authority) {
		this.authority = authority;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authority == null) ? 0 : authority.hashCode());
		result = prime * result
				+ ((userRoleId == null) ? 0 : userRoleId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		UserRole other = (UserRole) obj;
		if (authority == null) {
			if (other.authority != null)
				return false;
		} else if (!authority.equals(other.authority))
			return false;
		if (userRoleId == null) {
			if (other.userRoleId != null)
				return false;
		} else if (!userRoleId.equals(other.userRoleId))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return authority;
	}

}
