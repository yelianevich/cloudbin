package by.bsuir.cloudbin.model;

import java.io.Serializable;
import java.util.List;

/**
 * Class that incapsulates list of users
 * @author Raman Yelianevich
 *
 */
public final class Users implements Serializable{
	private static final long serialVersionUID = 7542031080056824440L;
	private List<User> users;
	
	public Users() { }
	
	public Users(List<User> users) {
		this.users = users;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public Long[] getIds() {
		Long[] ids = new Long[users.size()];
		int i = 0;
		for(User user : users) {
			ids[i++] = user.getUserId();
		}
		return ids;
	}
}
