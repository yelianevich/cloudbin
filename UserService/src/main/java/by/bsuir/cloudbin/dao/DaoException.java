package by.bsuir.cloudbin.dao;

public class DaoException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = -7597426500476808125L;

	/**
	 * Creates exception
	 */
	public DaoException() {
	}
	
	/**
	 * Creates exception and receive client's message
	 * @param message - message to create exception
	 */
	public DaoException(String message) {
		super(message);
	}
	
	/**
	 * Creates exception using exception cause
	 * @param cause - cause of exception creation
	 */
	public DaoException(Throwable cause) {
		super(cause);
	}
	
	/**
	 * Creates exception using message and cause
	 * @param message - message to create exception
	 * @param cause - cause of exception creation
	 */
	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}
}
