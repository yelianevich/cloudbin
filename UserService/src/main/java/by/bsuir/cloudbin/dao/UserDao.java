package by.bsuir.cloudbin.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;

import by.bsuir.cloudbin.model.User;
import by.bsuir.cloudbin.model.Users;
import by.bsuir.cloudbin.dao.DaoException;
import by.bsuir.cloudbin.util.HibernateUtilJpa;

/**
 * Concrete JPA data access object realization on user database
 * @author Raman_Yelianevich
 */
public final class UserDao implements IUserDao {
	private static final String REMOVE_EXCEPTION_MSG = "Can not remove users";
	private static final Logger LOG = Logger.getLogger(UserDao.class);
	private static final String JPQL_USERNAME_PARAM = "username";
	private static final String JPQL_SELECT_USERNAME =
			"SELECT user FROM User as user WHERE user.username = :" + JPQL_USERNAME_PARAM;

	public void saveOrUpdateUser(User user) throws DaoException {
		EntityTransaction transaction = null;
		EntityManager entityManager = HibernateUtilJpa.INSTANCE.getEntityManager();
		try {
			transaction = entityManager.getTransaction();
			transaction.begin();
			entityManager.merge(user);
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
			LOG.error(REMOVE_EXCEPTION_MSG, e);
			throw new DaoException(REMOVE_EXCEPTION_MSG, e);
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

	@SuppressWarnings("unchecked")
	public List<User> getAllUsers() {
		EntityManager entityManager = HibernateUtilJpa.INSTANCE.getEntityManager();
		return entityManager.createQuery("SELECT user From User as user").getResultList();
	}

	public User selectUserById(Long userId) {
		EntityManager entityManager = HibernateUtilJpa.INSTANCE.getEntityManager();
		return entityManager.find(User.class, userId);
	}
	
	@Override
	public User selectUserByUsername(String username) {
		EntityManager entityManager = HibernateUtilJpa.INSTANCE.getEntityManager();
		TypedQuery<User> query = entityManager.createQuery(JPQL_SELECT_USERNAME, User.class);
		query.setParameter(JPQL_USERNAME_PARAM, username);
		User user = query.getSingleResult();
		return user;
	}

	public void deleteUsers(Users users) throws DaoException {
		EntityTransaction transaction = null;
		Long[] ids = users.getIds();
		EntityManager entityManager = HibernateUtilJpa.INSTANCE.getEntityManager();
		try {
			transaction = entityManager.getTransaction();
			transaction.begin();
			Query query = entityManager.createQuery("DELETE FROM News news WHERE news.id IN (:ids)");
			query.setParameter("ids", ids);
			query.executeUpdate();
			transaction.commit();
		} catch (Exception e) {
			if (transaction != null && transaction.isActive()) {
				transaction.rollback();
			}
			LOG.error(REMOVE_EXCEPTION_MSG, e);
			throw new DaoException(REMOVE_EXCEPTION_MSG, e);
		} finally {
			if (entityManager != null && entityManager.isOpen()) {
				entityManager.close();
			}
		}
	}

}
