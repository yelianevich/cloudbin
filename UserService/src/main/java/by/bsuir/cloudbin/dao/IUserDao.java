package by.bsuir.cloudbin.dao;

import java.util.List;

import by.bsuir.cloudbin.model.User;
import by.bsuir.cloudbin.model.Users;
import by.bsuir.cloudbin.dao.DaoException;

/**
 * Interface for common operations on user data source 
 * @author Raman_Yelianevich
 */
public interface IUserDao {
	public void saveOrUpdateUser(User user) throws DaoException;
    public List<User> getAllUsers();
    public User selectUserById(Long userId);
    public void deleteUsers(Users users) throws DaoException;
    public User selectUserByUsername(String username);
}
